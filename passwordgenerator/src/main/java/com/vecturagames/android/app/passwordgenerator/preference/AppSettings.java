package com.vecturagames.android.app.passwordgenerator.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;

import com.vecturagames.android.app.passwordgenerator.MainApplication;
import com.vecturagames.android.app.passwordgenerator.R;
import com.vecturagames.android.app.passwordgenerator.enumeration.AppThemeType;
import com.vecturagames.android.app.passwordgenerator.util.Util;

public class AppSettings {

    public static final int MIN_PASSWORDS_COUNT = 1;
    public static final int MAX_PASSWORDS_COUNT = 99;
    public static final int MIN_PASSWORD_LENGTH = 1;
    public static final int MAX_PASSWORD_LENGTH = 999;
    public static final int DEFAULT_PASSWORDS_COUNT = 1;
    public static final int DEFAULT_PASSWORD_LENGTH = 18;
    public static final String DEFAULT_PASSWORD_SYMBOLS = "\'`\"!?,.:;$%&@~#()<>{}[]_*-+^=/|\\";

    public static final String PASSWORD_LOWER_CASE = "abcdefghijklmnopqrstuvwxyz";
    public static final String PASSWORD_UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String PASSWORD_NUMBERS = "0123456789";

    public static final String[] SIMILAR_CHARS = { "0O", "1lI" };

    private static final String SETTINGS_RATE_DIALOG_SHOWED = "SETTINGS_RATE_DIALOG_SHOWED";
    private static final String SETTINGS_APPLICATION_LAUNCH_COUNT = "SETTINGS_APPLICATION_LAUNCH_COUNT";
    private static final String SETTINGS_APPLICATION_LAST_VERSION_CODE = "SETTINGS_APPLICATION_LAST_VERSION_CODE";

    private static final String SETTINGS_APP_THEME = "SETTINGS_APP_THEME";
    private static final String SETTINGS_PASSWORD_USE_LOWER_CASE = "SETTINGS_PASSWORD_USE_LOWER_CASE";
    private static final String SETTINGS_PASSWORD_USE_UPPER_CASE = "SETTINGS_PASSWORD_USE_UPPER_CASE";
    private static final String SETTINGS_PASSWORD_USE_NUMBERS = "SETTINGS_PASSWORD_USE_NUMBERS";
    private static final String SETTINGS_PASSWORD_USE_SYMBOLS = "SETTINGS_PASSWORD_USE_SYMBOLS";
    private static final String SETTINGS_PASSWORD_SYMBOLS = "SETTINGS_PASSWORD_SYMBOLS";
    private static final String SETTINGS_PASSWORD_USE_UNIQUE_CHARS = "SETTINGS_PASSWORD_USE_UNIQUE_CHARS";
    private static final String SETTINGS_PASSWORD_USE_SIMILAR_CHARS = "SETTINGS_PASSWORD_USE_SIMILAR_CHARS";
    private static final String SETTINGS_PASSWORD_SEED = "SETTINGS_PASSWORD_SEED";
    private static final String SETTINGS_PASSWORDS_COUNT = "SETTINGS_PASSWORDS_COUNT";
    private static final String SETTINGS_PASSWORD_LENGTH = "SETTINGS_PASSWORD_LENGTH";

    private static AppSettings mInstance = null;

    public boolean mRateDialogShowed;
    public int mApplicationLaunchCount;
    public int mApplicationLastVersionCode;

    public AppThemeType mAppTheme;
    public boolean mPasswordUseLowerCase;
    public boolean mPasswordUseUpperCase;
    public boolean mPasswordUseNumbers;
    public boolean mPasswordUseSymbols;
    public String mPasswordSymbols;
    public boolean mPasswordUseUniqueChars;
    public boolean mPasswordUseSimilarChars;
    public String mPasswordSeed;
    public int mPasswordsCount;
    public int mPasswordLength;

    private SharedPreferences mPreferences = null;

    private static AppSettings createInstance() {
        mInstance = new AppSettings();
        return mInstance;
    }

    public static void destroyInstance() {
        mInstance = null;
    }

    public static AppSettings getInstance() {
        if (mInstance != null) {
            return mInstance;
        }
        else {
            return createInstance();
        }
    }

    private AppSettings() {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(MainApplication.getAppContext());

        setDefaultNonResetableSettings();
        setDefaultSettings();

        loadSettings();
    }

    private void setDefaultNonResetableSettings() {
        mRateDialogShowed = false;
        mApplicationLaunchCount = 0;
        mApplicationLastVersionCode = 0;
    }

    private void setDefaultSettings() {
        mAppTheme = AppThemeType.LIGHT;
        mPasswordUseLowerCase = true;
        mPasswordUseUpperCase = true;
        mPasswordUseNumbers = true;
        mPasswordUseSymbols = false;
        mPasswordSymbols = DEFAULT_PASSWORD_SYMBOLS;
        mPasswordUseUniqueChars = false;
        mPasswordUseSimilarChars = true;
        mPasswordSeed = "";
        mPasswordsCount = DEFAULT_PASSWORDS_COUNT;
        mPasswordLength = DEFAULT_PASSWORD_LENGTH;
    }

    public int getAppThemeId() {
        if (mAppTheme == AppThemeType.LIGHT) {
            return R.style.AppBaseTheme;
        }
        else {
            return R.style.AppBaseThemeDark;
        }
    }

    public int getColor(int id) {
        Context context = MainApplication.getAppContext();
        ContextThemeWrapper th = new ContextThemeWrapper(context, getAppThemeId());

        TypedValue typedValue = new TypedValue();
        th.getTheme().resolveAttribute(id, typedValue, true);

        if (typedValue.resourceId != 0) {
            id = typedValue.resourceId;
        }

        return ContextCompat.getColor(context, id);
    }

    public void saveSettings() {
        SharedPreferences.Editor editor = mPreferences.edit();

        editor.putBoolean(SETTINGS_RATE_DIALOG_SHOWED, mRateDialogShowed);
        editor.putInt(SETTINGS_APPLICATION_LAUNCH_COUNT, mApplicationLaunchCount);
        editor.putInt(SETTINGS_APPLICATION_LAST_VERSION_CODE, Util.getVersionCode(MainApplication.getAppContext()));
        editor.putInt(SETTINGS_APP_THEME, mAppTheme.ordinal());
        editor.putBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPasswordUseLowerCase);
        editor.putBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPasswordUseUpperCase);
        editor.putBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPasswordUseNumbers);
        editor.putBoolean(SETTINGS_PASSWORD_USE_SYMBOLS, mPasswordUseSymbols);
        editor.putString(SETTINGS_PASSWORD_SYMBOLS, mPasswordSymbols);
        editor.putBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPasswordUseUniqueChars);
        editor.putBoolean(SETTINGS_PASSWORD_USE_SIMILAR_CHARS, mPasswordUseSimilarChars);
        editor.putString(SETTINGS_PASSWORD_SEED, mPasswordSeed);
        editor.putInt(SETTINGS_PASSWORDS_COUNT, mPasswordsCount);
        editor.putInt(SETTINGS_PASSWORD_LENGTH, mPasswordLength);

        editor.apply();
    }

    private void loadSettings() {
        fixOldSettings();

        mRateDialogShowed = mPreferences.getBoolean(SETTINGS_RATE_DIALOG_SHOWED, mRateDialogShowed);
        mApplicationLaunchCount = mPreferences.getInt(SETTINGS_APPLICATION_LAUNCH_COUNT, mApplicationLaunchCount);
        mApplicationLastVersionCode = mPreferences.getInt(SETTINGS_APPLICATION_LAST_VERSION_CODE, mApplicationLastVersionCode);
        mAppTheme = AppThemeType.values()[mPreferences.getInt(SETTINGS_APP_THEME, mAppTheme.ordinal())];
        mPasswordUseLowerCase = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPasswordUseLowerCase);
        mPasswordUseUpperCase = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPasswordUseUpperCase);
        mPasswordUseNumbers = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPasswordUseNumbers);
        mPasswordUseSymbols = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_SYMBOLS, mPasswordUseSymbols);
        mPasswordSymbols = mPreferences.getString(SETTINGS_PASSWORD_SYMBOLS, mPasswordSymbols);
        mPasswordUseUniqueChars = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPasswordUseUniqueChars);
        mPasswordUseSimilarChars = mPreferences.getBoolean(SETTINGS_PASSWORD_USE_SIMILAR_CHARS, mPasswordUseSimilarChars);
        mPasswordSeed = mPreferences.getString(SETTINGS_PASSWORD_SEED, mPasswordSeed);
        mPasswordsCount = mPreferences.getInt(SETTINGS_PASSWORDS_COUNT, mPasswordsCount);
        mPasswordLength = mPreferences.getInt(SETTINGS_PASSWORD_LENGTH, mPasswordLength);
    }

    private void fixOldSettings() {
        SharedPreferences.Editor editor = mPreferences.edit();

        // Update of old version settings
        if (mPreferences.contains("SETTINGS_PASSWORD_LOWER_CASE")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPreferences.getBoolean("SETTINGS_PASSWORD_LOWER_CASE", mPasswordUseLowerCase));
            editor.remove("SETTINGS_PASSWORD_LOWER_CASE");
        }
        if (mPreferences.contains("SETTINGS_PASSWORD_UPPER_CASE")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPreferences.getBoolean("SETTINGS_PASSWORD_UPPER_CASE", mPasswordUseLowerCase));
            editor.remove("SETTINGS_PASSWORD_UPPER_CASE");
        }
        if (mPreferences.contains("SETTINGS_PASSWORD_NUMBERS")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPreferences.getBoolean("SETTINGS_PASSWORD_NUMBERS", mPasswordUseLowerCase));
            editor.remove("SETTINGS_PASSWORD_NUMBERS");
        }
        if (mPreferences.contains("SETTINGS_PASSWORD_UNIQUE_CHARS")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPreferences.getBoolean("SETTINGS_PASSWORD_UNIQUE_CHARS", mPasswordUseLowerCase));
            editor.remove("SETTINGS_PASSWORD_UNIQUE_CHARS");
        }

        editor.apply();
    }

}
