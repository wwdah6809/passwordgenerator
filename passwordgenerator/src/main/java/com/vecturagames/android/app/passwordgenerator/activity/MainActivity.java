package com.vecturagames.android.app.passwordgenerator.activity;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Random;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.vecturagames.android.app.passwordgenerator.BuildConfig;
import com.vecturagames.android.app.passwordgenerator.R;
import com.vecturagames.android.app.passwordgenerator.enumeration.AppThemeType;
import com.vecturagames.android.app.passwordgenerator.preference.AppSettings;
import com.vecturagames.android.app.passwordgenerator.util.Util;

public class MainActivity extends AppCompatActivity {

    private static final String CLIPBOARD_PASSWORD_LABEL = "password";
    private static final int RESTART_APPLICATION_DELAY = 2000;

    private Toolbar mToolbar = null;

    private LinearLayout mLinearLayoutPasswords = null;

    private CheckBox mCheckBoxLowerCase = null;
    private CheckBox mCheckBoxUpperCase = null;
    private CheckBox mCheckBoxNumbers = null;
    private CheckBox mCheckBoxSymbols = null;
    private CheckBox mCheckBoxUniqueChars = null;
    private CheckBox mCheckBoxSimilarChars = null;
    private CheckBox mCheckBoxSeed = null;

    private ArrayList<TextView> mTextViewsStrength = new ArrayList<TextView>();

    private EditText mEditTextSymbols = null;
    private EditText mEditTextPasswordsCount = null;
    private EditText mEditTextPasswordLength = null;
    private EditText mEditTextSeed = null;
    private ArrayList<EditText> mEditTextsPassword = new ArrayList<EditText>();

    private ArrayList<ProgressBar> mProgressBarsStrength = new ArrayList<ProgressBar>();

    private Button mButtonPasswordsCountMinus = null;
    private Button mButtonPasswordsCountPlus = null;
    private Button mButtonPasswordLengthMinus = null;
    private Button mButtonPasswordLengthPlus = null;
    private Button mButtonGenerate = null;
    private Button mButtonCopy = null;

    private Random mRandomSeed = null;
    private Random mRandomSecure = new SecureRandom();
    private String mLastSeed = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(AppSettings.getInstance().getAppThemeId());
        setContentView(R.layout.activity_main);

        AppSettings.getInstance().mApplicationLaunchCount++;

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // RelativeLayout
        mLinearLayoutPasswords = (LinearLayout)findViewById(R.id.linearLayoutPasswords);

        // EditText
        mEditTextSymbols = (EditText)findViewById(R.id.editTextSymbols);
        mEditTextPasswordsCount = (EditText)findViewById(R.id.editTextPasswordsCount);
        mEditTextPasswordLength = (EditText)findViewById(R.id.editTextPasswordLength);
        mEditTextSeed = (EditText)findViewById(R.id.editTextSeed);

        if (mEditTextSymbols != null) {
            mEditTextSymbols.setEnabled(AppSettings.getInstance().mPasswordUseSymbols);
            mEditTextSymbols.setText(AppSettings.getInstance().mPasswordSymbols);
            mEditTextSymbols.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    AppSettings.getInstance().mPasswordSymbols = editable.toString().trim();
                }
            });
        }

        if (mEditTextPasswordsCount != null) {
            mEditTextPasswordsCount.setText(String.valueOf(AppSettings.getInstance().mPasswordsCount));
            mEditTextPasswordsCount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        AppSettings.getInstance().mPasswordsCount = Util.clamp(Integer.valueOf(editable.toString()), AppSettings.MIN_PASSWORDS_COUNT, AppSettings.MAX_PASSWORDS_COUNT);
                    }
                    catch (NumberFormatException e) {
                        AppSettings.getInstance().mPasswordsCount = AppSettings.MIN_PASSWORDS_COUNT;
                    }
                    updatePasswordLayouts(false);
                    generateAndShowPasswords();
                }
            });
        }

        if (mEditTextPasswordLength != null) {
            mEditTextPasswordLength.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try {
                        AppSettings.getInstance().mPasswordLength = Util.clamp(Integer.valueOf(editable.toString()), AppSettings.MIN_PASSWORD_LENGTH, AppSettings.MAX_PASSWORD_LENGTH);
                    }
                    catch (NumberFormatException e) {
                        AppSettings.getInstance().mPasswordLength = AppSettings.MIN_PASSWORD_LENGTH;
                    }
                }
            });
            mEditTextPasswordLength.setText(String.valueOf(AppSettings.getInstance().mPasswordLength));
        }

        if (mEditTextSeed != null) {
            mEditTextSeed.setText(AppSettings.getInstance().mPasswordSeed);
            mEditTextSeed.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    AppSettings.getInstance().mPasswordSeed = editable.toString();
                }
            });
        }

        // CheckBox
        mCheckBoxLowerCase = (CheckBox)findViewById(R.id.checkBoxLowerCase);
        mCheckBoxUpperCase = (CheckBox)findViewById(R.id.checkBoxUpperCase);
        mCheckBoxNumbers = (CheckBox)findViewById(R.id.checkBoxNumbers);
        mCheckBoxSymbols = (CheckBox)findViewById(R.id.checkBoxCustomSymbols);
        mCheckBoxUniqueChars = (CheckBox)findViewById(R.id.checkBoxUniqueChars);
        mCheckBoxSimilarChars = (CheckBox)findViewById(R.id.checkBoxSimilarChars);
        mCheckBoxSeed = (CheckBox)findViewById(R.id.checkBoxSeed);

        if (mCheckBoxLowerCase != null) {
            mCheckBoxLowerCase.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseLowerCase = checked;
                }
            });
            mCheckBoxLowerCase.setChecked(AppSettings.getInstance().mPasswordUseLowerCase);
        }

        if (mCheckBoxUpperCase != null) {
            mCheckBoxUpperCase.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseUpperCase = checked;
                }
            });
            mCheckBoxUpperCase.setChecked(AppSettings.getInstance().mPasswordUseUpperCase);
        }

        if (mCheckBoxNumbers != null) {
            mCheckBoxNumbers.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseNumbers = checked;
                }
            });
            mCheckBoxNumbers.setChecked(AppSettings.getInstance().mPasswordUseNumbers);
        }

        if (mCheckBoxSymbols != null) {
            mCheckBoxSymbols.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseSymbols = checked;
                    mEditTextSymbols.setEnabled(checked);
                }
            });
            mCheckBoxSymbols.setChecked(AppSettings.getInstance().mPasswordUseSymbols);
        }

        if (mCheckBoxUniqueChars != null) {
            mCheckBoxUniqueChars.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseUniqueChars = checked;
                }
            });
            mCheckBoxUniqueChars.setChecked(AppSettings.getInstance().mPasswordUseUniqueChars);
        }

        if (mCheckBoxSimilarChars != null) {
            mCheckBoxSimilarChars.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    AppSettings.getInstance().mPasswordUseSimilarChars = checked;
                }
            });
            mCheckBoxSimilarChars.setChecked(AppSettings.getInstance().mPasswordUseSimilarChars);
        }

        if (mCheckBoxSeed != null) {
            mCheckBoxSeed.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton button, boolean checked) {
                    mEditTextSeed.setEnabled(checked);

                    if (checked) {
                        mLastSeed = null;
                    }
                    else {
                        AppSettings.getInstance().mPasswordSeed = "";
                    }
                }
            });
            mCheckBoxSeed.setChecked(!AppSettings.getInstance().mPasswordSeed.equals(""));
        }

        // Button
        mButtonPasswordsCountMinus = (Button)findViewById(R.id.buttonPasswordsCountMinus);
        mButtonPasswordsCountPlus = (Button)findViewById(R.id.buttonPasswordsCountPlus);
        mButtonPasswordLengthMinus = (Button)findViewById(R.id.buttonPasswordLengthMinus);
        mButtonPasswordLengthPlus = (Button)findViewById(R.id.buttonPasswordLengthPlus);
        mButtonGenerate = (Button)findViewById(R.id.buttonGenerate);
        mButtonCopy = (Button)findViewById(R.id.buttonCopy);

        if (mButtonPasswordsCountMinus != null) {
            mButtonPasswordsCountMinus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppSettings.getInstance().mPasswordsCount > AppSettings.MIN_PASSWORDS_COUNT) {
                        mEditTextPasswordsCount.setText(String.valueOf(AppSettings.getInstance().mPasswordsCount - 1));
                    }
                }
            });
        }

        if (mButtonPasswordsCountPlus != null) {
            mButtonPasswordsCountPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppSettings.getInstance().mPasswordsCount < AppSettings.MAX_PASSWORDS_COUNT) {
                        mEditTextPasswordsCount.setText(String.valueOf(AppSettings.getInstance().mPasswordsCount + 1));
                    }
                }
            });
        }

        if (mButtonPasswordLengthMinus != null) {
            mButtonPasswordLengthMinus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppSettings.getInstance().mPasswordLength > AppSettings.MIN_PASSWORD_LENGTH) {
                        mEditTextPasswordLength.setText(String.valueOf(AppSettings.getInstance().mPasswordLength - 1));
                    }
                }
            });
        }

        if (mButtonPasswordLengthPlus != null) {
            mButtonPasswordLengthPlus.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppSettings.getInstance().mPasswordLength < AppSettings.MAX_PASSWORD_LENGTH) {
                        mEditTextPasswordLength.setText(String.valueOf(AppSettings.getInstance().mPasswordLength + 1));
                    }
                }
            });
        }

        if (mButtonGenerate != null) {
            mButtonGenerate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    generateAndShowPasswords();
                }
            });
        }

        if (mButtonCopy != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mButtonCopy.setOnClickListener(new OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(View v) {
                        String passwords = "";
                        String append = (mEditTextsPassword.size() > 1) ? "\n" : "";
                        for (int i = 0; i < mEditTextsPassword.size(); i++) {
                            passwords += mEditTextsPassword.get(i).getText().toString() + append;
                        }

                        ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText(CLIPBOARD_PASSWORD_LABEL, passwords);
                        clipboard.setPrimaryClip(clip);
                        if (mEditTextsPassword.size() > 1) {
                            Toast.makeText(MainActivity.this, R.string.main_activity_info_copied_to_clipboard_plural, Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(MainActivity.this, R.string.main_activity_info_copied_to_clipboard_singular, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            else {
                mButtonCopy.setVisibility(View.GONE);
            }
        }

        updateCharactersText();

        updatePasswordLayouts(true);
        generateAndShowPasswords();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppSettings.destroyInstance();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppSettings.getInstance().saveSettings();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateCharactersText();
    }

    @Override
    public void onBackPressed() {
        if (AppSettings.getInstance() != null && (!AppSettings.getInstance().mRateDialogShowed && AppSettings.getInstance().mApplicationLaunchCount >= 5)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.format(getString(R.string.dialog_quit_rate), getString(R.string.app_name)));
            builder.setPositiveButton(getString(R.string.dialog_button_rate_it), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AppSettings.getInstance().mRateDialogShowed = true;
                    Util.openPlayStore(MainActivity.this, BuildConfig.APPLICATION_ID, 0);
                    finish();
                }
            });
            builder.setNegativeButton(getString(R.string.dialog_button_no_thanks), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AppSettings.getInstance().mRateDialogShowed = true;
                    finish();
                }
            });
            builder.show();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_reset_symbols:
                mEditTextSymbols.setText(AppSettings.DEFAULT_PASSWORD_SYMBOLS);
                return true;

            case R.id.menu_app_theme:
                showAppThemeDialog();
                return true;

            case R.id.menu_tips:
                AlertDialog.Builder builderTips = new AlertDialog.Builder(this);
                builderTips.setTitle(R.string.main_activity_menu_tips);
                builderTips.setMessage(R.string.dialog_tips_text);
                builderTips.setPositiveButton(R.string.dialog_button_ok, null);
                builderTips.show();
                return true;

            case R.id.menu_password_strength:
                AlertDialog.Builder builderStrength = new AlertDialog.Builder(this);
                builderStrength.setTitle(R.string.main_activity_menu_password_strength);
                builderStrength.setMessage(String.format(getString(R.string.dialog_password_strength_text), getString(R.string.app_name), getString(R.string.app_name)));
                builderStrength.setPositiveButton(R.string.dialog_button_ok, null);
                builderStrength.show();
                return true;

            case R.id.menu_help:
                AlertDialog.Builder builderHelp = new AlertDialog.Builder(this);
                builderHelp.setTitle(R.string.main_activity_menu_help);
                builderHelp.setMessage(getString(R.string.dialog_help_text));
                builderHelp.setPositiveButton(R.string.dialog_button_ok, null);
                builderHelp.show();
                return true;

            case R.id.menu_about:
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAppThemeDialog() {
        final AppThemeType originalAppTheme = AppSettings.getInstance().mAppTheme;

        String[] appThemes = getResources().getStringArray(R.array.app_theme);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.main_activity_menu_app_theme));
        builder.setSingleChoiceItems(appThemes, AppSettings.getInstance().mAppTheme.ordinal(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppSettings.getInstance().mAppTheme = AppThemeType.values()[which];
            }
        });
        builder.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppSettings.getInstance().mAppTheme = originalAppTheme;
            }
        });
        builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (originalAppTheme != AppSettings.getInstance().mAppTheme) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(String.format(getString(R.string.dialog_restart_theme_change), getString(R.string.app_name)));
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            restartApplication(RESTART_APPLICATION_DELAY);
                        }
                    });
                    builder.show();
                }
            }
        });
        builder.show();
    }

    private void restartApplication(int delay) {
        PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(getIntent()), PendingIntent.FLAG_ONE_SHOT);
        AlarmManager manager = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        finish();
    }

    private void updatePasswordLayouts(boolean focusFirst) {
        mLinearLayoutPasswords.removeAllViews();

        mTextViewsStrength.clear();
        mProgressBarsStrength.clear();
        mEditTextsPassword.clear();

        for (int i = 0; i < AppSettings.getInstance().mPasswordsCount; i++) {
            LinearLayout layoutPassword = (LinearLayout)getLayoutInflater().inflate(R.layout.layout_password, null);
            mLinearLayoutPasswords.addView(layoutPassword);

            // TextView
            TextView textViewStrength = (TextView)layoutPassword.findViewById(R.id.textViewStrength);

            // ProgressBar
            ProgressBar progressBarStrength = (ProgressBar)layoutPassword.findViewById(R.id.progressBarStrength);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mProgressBarStrength.setScaleY(1.5f);
            }*/

            // EditText
            EditText editTextPassword = (EditText)layoutPassword.findViewById(R.id.editTextPassword);

            if (editTextPassword != null) {
                editTextPassword.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void afterTextChanged(Editable userEnteredValue) {

                    }

                    @Override
                    public void onTextChanged(CharSequence userEnteredValue, int start, int before, int count) {
                        final String[] passwordStrengths = getResources().getStringArray(R.array.other_password_strength);

                        int passwordEntropy = getPasswordEntropy(editTextPassword.getText().toString());
                        String passwordStrength;
                        int passwordStrengthIdx;
                        int color;

                        if (passwordEntropy >= 170) {
                            passwordStrengthIdx = 4;
                            color = AppSettings.getInstance().getColor(R.attr.password_strength_ultra_strong);
                        }
                        else if (passwordEntropy >= 112) {
                            passwordStrengthIdx = 3;
                            color = AppSettings.getInstance().getColor(R.attr.password_strength_very_strong);
                        }
                        else if (passwordEntropy >= 80) {
                            passwordStrengthIdx = 2;
                            color = AppSettings.getInstance().getColor(R.attr.password_strength_strong);
                        }
                        else if (passwordEntropy >= 48) {
                            passwordStrengthIdx = 1;
                            color = AppSettings.getInstance().getColor(R.attr.password_strength_medium);
                        }
                        else {
                            passwordStrengthIdx = 0;
                            color = AppSettings.getInstance().getColor(R.attr.password_strength_weak);
                        }

                        passwordStrength = passwordStrengths[passwordStrengthIdx];

                        textViewStrength.setTextColor(color);
                        textViewStrength.setText(getString(R.string.main_activity_textview_strength, passwordStrength, passwordEntropy));
                        progressBarStrength.getProgressDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN);
                        progressBarStrength.setProgress(Math.min(Math.round(passwordEntropy * (100.0f / 112.0f)), 100));
                    }

                    @Override
                    public void beforeTextChanged(CharSequence userEnteredValue, int start, int before, int count) {

                    }
                });
                if (i == 0 && focusFirst) {
                    editTextPassword.requestFocus();
                }
            }

            mTextViewsStrength.add(textViewStrength);
            mProgressBarsStrength.add(progressBarStrength);
            mEditTextsPassword.add(editTextPassword);
        }
    }

    private void updateCharactersText() {
        Point size = Util.getDisplaySize(this);
        int widthDp = Util.pxToDp(Util.getDisplayMetrics(this), size.x);

        if (mCheckBoxUniqueChars != null) {
            if (widthDp < 400) {
                mCheckBoxUniqueChars.setText(R.string.main_activity_chechbox_unique_chars);
            }
            else {
                mCheckBoxUniqueChars.setText(R.string.main_activity_chechbox_unique_characters);
            }
        }

        if (mCheckBoxSimilarChars != null) {
            if (widthDp < 400) {
                mCheckBoxSimilarChars.setText(R.string.main_activity_chechbox_similar_chars);
            }
            else {
                mCheckBoxSimilarChars.setText(R.string.main_activity_chechbox_similar_characters);
            }
        }
    }

    private int getPasswordEntropy(String password) {
        int n = 0, uniqueCharacters = 0;

        for (int i = 0; i < password.length(); i++) {
            if (i == password.indexOf(password.charAt(i))) {
                uniqueCharacters++;
            }
        }

        boolean hasLowerCase = false, hasUpperCase = false, hasNumbers = false;
        String otherCharacters = "";

        for (int i = 0; i < password.length(); i++) {
            String character = String.valueOf(password.charAt(i));

            if (!hasLowerCase && AppSettings.PASSWORD_LOWER_CASE.contains(character)) {
                n += AppSettings.PASSWORD_LOWER_CASE.length();
                hasLowerCase = true;
            }
            else if (!hasUpperCase && AppSettings.PASSWORD_UPPER_CASE.contains(character)) {
                n += AppSettings.PASSWORD_UPPER_CASE.length();
                hasUpperCase = true;
            }
            else if (!hasNumbers && AppSettings.PASSWORD_NUMBERS.contains(character)) {
                n += AppSettings.PASSWORD_NUMBERS.length();
                hasNumbers = true;
            }
            else if (!otherCharacters.contains(String.valueOf(character))) {
                n++;
                otherCharacters += String.valueOf(character);
            }
        }

        int passwordEntropy = (int)(uniqueCharacters * (Math.log10(n) / Math.log10(2)));
        return (passwordEntropy == 0 && !password.isEmpty()) ? 1 : passwordEntropy;
    }

    private void generateAndShowPasswords() {
        for (int i = 0; i < mEditTextsPassword.size(); i++) {
            String password = generatePassword(AppSettings.getInstance().mPasswordLength, mEditTextSeed.getText().toString());

            if (password.compareTo("") != 0) {
                mEditTextsPassword.get(i).setText(password);
                if (i == 0) {
                    mEditTextsPassword.get(i).setSelection(mEditTextsPassword.get(i).getText().length());
                }
            }
        }
    }

    private String generatePassword(int length, String seed) {
        if (mCheckBoxSeed.isChecked() && (mLastSeed == null || seed.compareTo(mLastSeed) != 0)) {
            mRandomSeed = new SecureRandom(seed.getBytes());
            mLastSeed = seed;
        }

        Random random = mCheckBoxSeed.isChecked() ? mRandomSeed : mRandomSecure;

        int uniqueCharactersCount = 0;

        String characters = "";
        if (mCheckBoxLowerCase.isChecked()) {
            characters += AppSettings.PASSWORD_LOWER_CASE;
            uniqueCharactersCount += AppSettings.PASSWORD_LOWER_CASE.length();
        }
        if (mCheckBoxUpperCase.isChecked()) {
            characters += AppSettings.PASSWORD_UPPER_CASE;
            uniqueCharactersCount += AppSettings.PASSWORD_UPPER_CASE.length();
        }
        if (mCheckBoxNumbers.isChecked()) {
            characters += AppSettings.PASSWORD_NUMBERS;
            uniqueCharactersCount += AppSettings.PASSWORD_NUMBERS.length();
        }
        if (mCheckBoxSymbols.isChecked()) {
            String symbols = mEditTextSymbols.getText().toString().trim();
            String uniqueCharacters = characters;

            for (int i = 0; i < symbols.length(); i++) {
                String symbol = String.valueOf(symbols.charAt(i));

                if (!uniqueCharacters.contains(symbol)) {
                    uniqueCharacters += symbol;
                    uniqueCharactersCount++;
                }
            }

            characters += symbols;
        }

        if (!mCheckBoxSimilarChars.isChecked()) {
            for (int i = 0; i < AppSettings.SIMILAR_CHARS.length; i++) {
                String currentSimilarChars = AppSettings.SIMILAR_CHARS[i];

                for (int j = 0; j < currentSimilarChars.length(); j++) {
                    int countOriginal = characters.length();
                    characters = characters.replaceAll(String.valueOf(currentSimilarChars.charAt(j)), "");

                    if (countOriginal > characters.length()) {
                        uniqueCharactersCount--;
                    }
                }
            }
        }

        String password = "";

        if (!characters.equals("")) {
            length = Math.max(length, AppSettings.MIN_PASSWORD_LENGTH);

            for (int i = 0; i < length; i++) {
                int rnd = random.nextInt(characters.length());
                String c = String.valueOf(characters.charAt(rnd));

                if (mCheckBoxUniqueChars.isChecked() && password.length() < uniqueCharactersCount) {
                    while (password.contains(c)) {
                        rnd = random.nextInt(characters.length());
                        c = String.valueOf(characters.charAt(rnd));
                    }
                }

                password += c;
            }
        }

        return password;
    }

}
